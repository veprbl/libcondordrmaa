/***************************************************************
 *
 * Copyright (C) 1990-2008, Condor Team, Computer Sciences Department,
 * University of Wisconsin-Madison, WI.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you
 * may not use this file except in compliance with the License.  You may
 * obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ***************************************************************/

#ifndef CONDOR_DRMAA_COMMON_H
#define CONDOR_DRMAA_COMMON_H

#include <stdarg.h>
#include "auxDrmaa.h"

#define SCHEDD_NAME_BUFSIZE 1024
extern char schedd_name[];

#define FILEDIR_BUFSIZE 1024
extern char file_dir[];

extern MUTEX_TYPE job_list_lock;
extern condor_drmaa_job_info_t *job_list;
extern int num_jobs;

extern MUTEX_TYPE iniparser_lock;
extern MUTEX_TYPE session_lock;
extern int session_lock_initialized;

bool is_debug();
#ifdef __GNUC__
void debug_print(const char *format, ...) __attribute__((format(printf, 1, 2)));
#else
void debug_print(const char *format, ...);
#endif

#endif /* CONDOR_DRMAA_COMMON_H */
