# Microsoft Developer Studio Generated NMAKE File, Based on libdrmaa.dsp
!IF "$(CFG)" == ""
CFG=LIBDRMAA - WIN32 RELEASE
!MESSAGE No configuration specified. Defaulting to LIBDRMAA - WIN32 RELEASE.
!ENDIF 

!IF "$(CFG)" != "libdrmaa - Win32 Release" && "$(CFG)" != "libdrmaa - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "libdrmaa.mak" CFG="LIBDRMAA - WIN32 RELEASE"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "libdrmaa - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libdrmaa - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "libdrmaa - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\libdrmaa.dll"


CLEAN :
	-@erase "$(INTDIR)\auxDrmaa.obj"
	-@erase "$(INTDIR)\drmaa_common.obj"
	-@erase "$(INTDIR)\iniparser.obj"
	-@erase "$(INTDIR)\libDrmaa.obj"
	-@erase "$(INTDIR)\my_popen.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\libdrmaa.dll"
	-@erase "$(OUTDIR)\libdrmaa.exp"
	-@erase "$(OUTDIR)\libdrmaa.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MT /W3 /GX /O2 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "LIBDRMAA_EXPORTS" /D "CONDOR_DRMAA_STANDALONE" /D SYSCONFDIR=\"c:\\drmaa\" /D "DRMAA_DLL" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libdrmaa.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\libdrmaa.pdb" /machine:I386 /out:"$(OUTDIR)\libdrmaa.dll" /implib:"$(OUTDIR)\libdrmaa.lib" 
LINK32_OBJS= \
	"$(INTDIR)\auxDrmaa.obj" \
	"$(INTDIR)\drmaa_common.obj" \
	"$(INTDIR)\iniparser.obj" \
	"$(INTDIR)\libDrmaa.obj" \
	"$(INTDIR)\my_popen.obj"

"$(OUTDIR)\libdrmaa.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "libdrmaa - Win32 Debug"

OUTDIR=.\libdrmaa___Win32_Debug
INTDIR=.\libdrmaa___Win32_Debug
# Begin Custom Macros
OutDir=.\libdrmaa___Win32_Debug
# End Custom Macros

ALL : "$(OUTDIR)\libdrmaa.dll"


CLEAN :
	-@erase "$(INTDIR)\auxDrmaa.obj"
	-@erase "$(INTDIR)\drmaa_common.obj"
	-@erase "$(INTDIR)\iniparser.obj"
	-@erase "$(INTDIR)\libDrmaa.obj"
	-@erase "$(INTDIR)\my_popen.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\libdrmaa.dll"
	-@erase "$(OUTDIR)\libdrmaa.exp"
	-@erase "$(OUTDIR)\libdrmaa.lib"
	-@erase "$(OUTDIR)\libdrmaa.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MT /W3 /GX /Zi /O2 /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "LIBDRMAA_EXPORTS" /D "CONDOR_DRMAA_STANDALONE" /D SYSCONFDIR=\"c:\\drmaa\" /D "DRMAA_DLL" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libdrmaa.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\libdrmaa.pdb" /debug /machine:I386 /out:"$(OUTDIR)\libdrmaa.dll" /implib:"$(OUTDIR)\libdrmaa.lib" 
LINK32_OBJS= \
	"$(INTDIR)\auxDrmaa.obj" \
	"$(INTDIR)\drmaa_common.obj" \
	"$(INTDIR)\iniparser.obj" \
	"$(INTDIR)\libDrmaa.obj" \
	"$(INTDIR)\my_popen.obj"

"$(OUTDIR)\libdrmaa.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("libdrmaa.dep")
!INCLUDE "libdrmaa.dep"
!ELSE 
!MESSAGE Warning: cannot find "libdrmaa.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "libdrmaa - Win32 Release" || "$(CFG)" == "libdrmaa - Win32 Debug"
SOURCE=.\auxDrmaa.c

"$(INTDIR)\auxDrmaa.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\drmaa_common.c

"$(INTDIR)\drmaa_common.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\iniparser.c

!IF  "$(CFG)" == "libdrmaa - Win32 Release"

CPP_SWITCHES=/nologo /MT /W3 /GX /O2 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "LIBDRMAA_EXPORTS" /D "CONDOR_DRMAA_STANDALONE" /D SYSCONFDIR=\"c:\\drmaa\" /D "DRMAA_DLL" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\iniparser.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "libdrmaa - Win32 Debug"

CPP_SWITCHES=/nologo /MT /W3 /GX /Zi /O2 /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "LIBDRMAA_EXPORTS" /D "CONDOR_DRMAA_STANDALONE" /D SYSCONFDIR=\"c:\\drmaa\" /D "DRMAA_DLL" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\iniparser.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ENDIF 

SOURCE=.\libDrmaa.c

"$(INTDIR)\libDrmaa.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\my_popen.c

"$(INTDIR)\my_popen.obj" : $(SOURCE) "$(INTDIR)"



!ENDIF 

