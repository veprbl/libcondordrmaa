/***************************************************************
 *
 * Copyright (C) 1990-2008, Condor Team, Computer Sciences Department,
 * University of Wisconsin-Madison, WI.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you
 * may not use this file except in compliance with the License.  You may
 * obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ***************************************************************/

/* Global variables used throughout the drmaa library */

#include <stdarg.h>
#include "auxDrmaa.h"
#include "drmaa_common.h"

#ifdef WIN32
MUTEX_TYPE session_lock;
int session_lock_initialized = 0;
#else
MUTEX_TYPE session_lock = PTHREAD_MUTEX_INITIALIZER;
int session_lock_initialized = 1;
#endif

int session_state = INACTIVE;

// The following are touched by: 
//   1) drmaa_run_jobs() - to add a job
//   2) drmaa_run_bulk_jobs() - to add a job
//   3) drmaa_control() - verifying job_id validity
//   4) drmaa_job_ps() - verifying job_id validity
//   5) drmaa_synchronize() / drmaa_wait() - to copy jobs to internal lists and
//      increment reference counters
MUTEX_TYPE job_list_lock;
condor_drmaa_job_info_t *job_list = NULL;
int num_jobs = 0;

// iniparser is not thread-safe
MUTEX_TYPE iniparser_lock;

char schedd_name[1024] = "";
char file_dir[1024] = "";

bool is_debug()
{
	char *debug_env = getenv("LIBCONDORDRMAA_DEBUG");

	if (debug_env == NULL) return false;

	return atoi(debug_env);
}

void
debug_print(const char *format, ...)
{
	va_list ap;

	if (!is_debug()) return;

	fprintf(stderr, "DEBUG: ");
	va_start(ap, format);
	vfprintf(stderr, format, ap);
	va_end(ap);
}
